//-----------Dark Mode---------------//
function toggleDarkMode() {
    let divh = document.getElementById("divh");

    let body = document.body;

    body.classList.toggle('dark-mode');

    if(body.classList.contains("dark-mode")){   
        localStorage.setItem('modoOscuroActivado', 'true');
        divh.title = "Activar Modo Claro"
    }
    else{      
        localStorage.setItem('modoOscuroActivado', 'false');
        divh.title = "Activar Modo Ocuro"
    }    
}
divh.title = "Cambiar Modo";

document.addEventListener('DOMContentLoaded', function() {
    let modoOscuroActivado = localStorage.getItem('modoOscuroActivado');
    if (modoOscuroActivado === 'true') {
        // Activar el modo oscuro si está almacenado como activado
        document.body.classList.add('dark-mode');
    }
});


//------------------Función scroll nav fijo-----------------------------
window.onscroll = function() {scrollFunction()};

    var nav = document.getElementById("nav");
    var sticky = nav.offsetTop;
    
    function scrollFunction() {
        if (window.pageYOffset > sticky) {
            nav.classList.add("fixed");
        } else {
            nav.classList.remove("fixed");
        }
}



//-----------------Buscador del inicio------------------------------
function buscarIndex(){
    let buscar = document.getElementById("buscar").value;
    let mensajeIndex = document.getElementById("mensajeIndex");

    buscarMin = buscar.toLowerCase();
    
    switch(buscarMin){
        case "nosotros":
        case "Iinstitucional":
            window.location = "../html/institucional.html";
            break;
        case "personal":
        case "perfil":
            window.location = "../html/perfil.html";
            break;
        case "posters":
        case "publicaciones":
            window.location = "../html/poster.html";
            break;
        case "modificar perfil":
        case "actualizar datos":
            window.location = "../html/form.html"
            break;
        case "registro":
        case "nuevo usuario":
            window.location = "../html/registro.html"
            break;
        case "":
            mensajeIndex.innerHTML = ""
            break;
        default:
            mensajeIndex.textContent = `No se encontró información sobre "${buscar}", Intente buscar una palabra clave.`
    }
}


function handleKeyPress(event) {
    if (event.key === 'Enter') {
      event.preventDefault(); // Evita que el formulario se envíe
      buscarIndex(); // Llama a la función que deseas ejecutar
    }
}


//----------------------Verifica que la nueva y verificada contraseña son iguales------------------------//

async function verificarCtr(event) {
    event.preventDefault(); // Evitar que el formulario se envíe de la manera tradicional

    const crtActual = document.getElementById("crtActual").value;
    const ctrNueva = document.getElementById("ctrNueva");
    const ctrVerificada = document.getElementById("ctrVerificada").value;
    const pFormulario = document.getElementById("pFormulario");
    const cantidad = ctrNueva.value.length;

    try {
        // Cargar el archivo JSON con los datos de los usuarios
        const response = await fetch('../js/usuarios.json');
        const usuarios = await response.json();

        // Verificar la contraseña actual
        const usuarioValido = usuarios.find(user => user.password === crtActual);

        if (usuarioValido) {
            if (ctrNueva.value === "") {
                pFormulario.textContent = "La contraseña nueva está vacía";
                pFormulario.style.color = "red";
            } else if (ctrNueva.value !== ctrVerificada) {
                pFormulario.textContent = "¡Las contraseñas no coinciden, verifíquelas por favor!";
                pFormulario.style.color = "red";
            } else if (cantidad <= 8) {
                pFormulario.textContent = "¡La contraseña nueva debe tener más de 8 caracteres!";
                pFormulario.style.color = "red";
            } else {
                pFormulario.textContent = "";
                alert("¡Información actualizada con éxito!");
                window.location = "./perfil.html";
            }
        } else {
            pFormulario.textContent = "¡La contraseña actual no es correcta!";
            pFormulario.style.color = "red";
        }
    } catch (error) {
        console.error('Error al cargar el archivo JSON', error);
        pFormulario.textContent = "Hubo un error al intentar verificar la contraseña.";
        pFormulario.style.color = "red";
    }
}




//---------------------------Registrar usuario-----------------------------------

function registroUsu(){
    let ctrUsuario = document.getElementById("ctrUsuario");
    let nomUsuario = document.getElementById("nomUsuario").value;
    let pRegistro = document.getElementById("pRegistro");
    let Username = document.getElementById("Username").value;
    let cantidad = ctrUsuario.value.length;


    pRegistro.innerHTML = ""

    var camposRequeridos = document.querySelectorAll('#formularioReg [required]');

    if(ctrUsuario.value===""){
        pRegistro.textContent = "Ingrese una contraseña valida"
        pRegistro.style = "color: red"
    }
    else
        if(cantidad <= 8 ){
            pRegistro.textContent = "La contraseña debe tener más de 8 carácteres";
            pRegistro.style = "color: red"
            return false
        }
        else{
            
            // Variable para verificar si todos los campos están llenos
            var todosLlenos = true;

            // Iterar sobre los campos requeridos y verificar si están llenos
            camposRequeridos.forEach(function(campo) {
                if (campo.value.trim() === '') {
                    todosLlenos = false;
                }
            });

            // Si no todos los campos están llenos, mostrar mensaje de alerta
            if (!todosLlenos) {
                window.alert("Termina de llenar los campos requeridos")
                return false; // Evitar que el formulario se envíe
            }
            sessionStorage.setItem('isLoggedIn', 'true');
            window.alert("Bienvenid@ "+ Username + ". Su cuenta ha sido registrada exitosamente.")
            window.location="./perfil.html"
        }
    }




//---------------------Enviar comentarios whatsapp---------------------//

function enviarmensaje(){
    let mensajeIngresado = document.getElementById("mensajeIngresado").value;

    window.open(`https://wa.me/+573215569549?text=${mensajeIngresado}`, "_blank")
}



//----------------------Cambiar foto de perfil en perfil--------------//

function cambiarPicture() {
    document.getElementById("imageSubida").click();

    document.getElementById("imageSubida").addEventListener("change", function() {
        const file = this.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                const fotoPerfil = document.getElementById("fotoPerfil");
                fotoPerfil.src = e.target.result;
                localStorage.setItem('fotoPerfil', e.target.result);
            }
            reader.readAsDataURL(file);
        }
    });
}

function inputFile() {
    let fotoResul = document.getElementById("fotoResul");
    document.getElementById('inputfoto').click();

    document.getElementById('inputfoto').addEventListener('change', function() {
        const file = this.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                fotoResul.src = e.target.result;
                fotoResul.style.visibility = 'visible';
                fotoResul.style.borderRadius = '50%';
                fotoResul.style.border = 'solid 1px #000';
                fotoResul.title = file.name;
                // También actualizamos la foto de perfil en localStorage
                localStorage.setItem('fotoPerfil', e.target.result);
            }
            reader.readAsDataURL(file);
        }
    });
}

// Función que se ejecuta cuando se carga la página de la foto de perfil
function loadFotoPerfil() {
    const storedImage = localStorage.getItem('fotoPerfil');
    if (storedImage) {
        if (document.getElementById('fotoPerfil')) {
            document.getElementById('fotoPerfil').src = storedImage;
        }
    }
}

// Función que se ejecuta cuando se carga la página del formulario
function loadFormulario() {
    const storedImage = localStorage.getItem('fotoPerfil');
    if (storedImage) {
        // Inicialmente ocultamos la imagen en el formulario
        document.getElementById('fotoResul').style.visibility = 'hidden';
    }
}

// Función que se ejecuta cuando se carga la página del formulario y de la foto de perfil
window.onload = function() {
    if (document.getElementById('fotoPerfil')) {
        loadFotoPerfil();
    }
    if (document.getElementById('fotoResul')) {
        loadFormulario();
    }
}





//----------------------------------------------------

//-----------------------------------------------

var section = document.getElementById("SectionPoster");

var botones = section.querySelectorAll("button");

// Iterar sobre la lista de botones y agregar un evento clic a cada uno
botones.forEach(function(boton) {
    // Aquí agregamos el manejador de eventos para el clic
    boton.addEventListener("click", function() {
        // Obtener el número del botón a través de su ID
        var numeroBoton = boton.id.replace("boton", "");
        // Aquí puedes hacer lo que necesites con el número del botón
        
        switch(numeroBoton){
            case "imagen1":
                window.location = "../html/perfil.html"
                break;
            case "imagen2":
                window.location = "../index.html"
                break;
            case "imagen3":
                window.location = "../html/institucional.html"
                break;
            case "imagen4":
                window.location = "../html/form.html"
                break;
            case "form":
                window.location = "../html/"+numeroBoton+".html"
                break;
            default:
                window.location = "https://sena.territorio.la/cms/index.php"
                break;
        }
    });
});

async function loguear() {
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let datosIncorrectos = document.getElementById("datosIncorrectos");

    try {
        // Cargar el JSON con los datos de los usuarios
        const response = await fetch('./js/usuarios.json');
        const usuarios = await response.json();

        // Verificar los datos ingresados
        const usuarioValido = usuarios.find(user => user.username === username && user.password === password);

        if (usuarioValido) {
            sessionStorage.setItem('isLoggedIn', 'true');
            window.location = "./html/inicio.html";
        } else {
            datosIncorrectos.textContent = "Los datos ingresados son incorrectos";
            datosIncorrectos.style.color = "red";
        }
    } catch (error) {
        console.error('Error al cargar el archivo JSON', error);
        datosIncorrectos.textContent = "Los datos ingresados son incorrectos";
        datosIncorrectos.style.color = "red";
    }
}

function logout() {
    // Eliminar el indicador de sesión
    sessionStorage.removeItem('isLoggedIn');
    // Redirigir al usuario a la página de inicio de sesión
    window.location.href = "../index.html";
}

function CambiarPass(elementId, buttonId, relativePath = false) {
    let password = document.getElementById(elementId);
    let buttonPass = document.getElementById(buttonId);

    if (password.type === "password") {
        password.type = "text";
        buttonPass.src = relativePath ? "../img/ver.png" : "./img/ver.png"; // Usa la ruta adecuada
    } else {
        password.type = "password";
        buttonPass.src = relativePath ? "../img/ojo.png" : "./img/ojo.png"; // Usa la ruta adecuada
    }
}


