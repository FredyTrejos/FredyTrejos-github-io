
//-----------------------------------------------

var section = document.getElementById("SectionPoster");

var botones = section.querySelectorAll("button");

// Iterar sobre la lista de botones y agregar un evento clic a cada uno
botones.forEach(function(boton) {
    // Aquí agregamos el manejador de eventos para el clic
    boton.addEventListener("click", function() {
        // Obtener el número del botón a través de su ID
        var numeroBoton = boton.id.replace("boton", "");
        // Aquí puedes hacer lo que necesites con el número del botón
        
        switch(numeroBoton){
            case "imagen1":
                window.location = "../html/perfil.html"
                break;
            case "imagen2":
                window.location = "../index.html"
                break;
            case "imagen3":
                window.location = "../html/institucional.html"
                break;
            case "imagen4":
                window.location = "../html/form.html"
                break;
            case "form":
                window.location = "../html/"+numeroBoton+".html"
                break;
            default:
                window.location = "https://sena.territorio.la/cms/index.php"
                break;

        }
    });
});



