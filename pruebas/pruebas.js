    //Si el mayor de los dos no es múltiplo del otro
//vamos probando multiplos del mayor hasta que sea
//también múltiplo del menor.

function mcm2(a, b) {
    let mul = Math.max(a, b);
    let inc = mul;

    while (mul % a != 0 || mul % b != 0) {
        mul += inc;
    }
    
    console.log(mul);
}

function hola(a){
    console.log("Hola " + a+ " cómo estás?");
}

//La función recibe como argumento una lista de números positivos
//Devuelve otra lista con los números que están repetidos
function repetidos(lista){
    //Ordeno la lista (son números por eso lafunción callback)

    let ordenada = lista.sort(function(a, b){return a - b});
    let repes=[];
    let item = 0;
    let existe;
    //Recorro la lista
    while (item < ordenada.length){
      existe = false;
      //Si un elemento es igual al siguiente avanzo el índice
      //continuo incremetando el indice hasta encontar un
      //elemento diferente
      existe = ordenada[item] == ordenada[++item];
      //si un bloque vacío, no hay que hacer nada.
      while (ordenada[item] == ordenada[++item] && existe){ }
      //Hay un elemento repetido: anotar en  lista de repetidos
      if (existe){
        repes.push(ordenada[item-1])
      }
    }
    return repes;

    
  }  
let numeros = [2, 3, 5, 2, 7, 3, 5, 8, 8, 8, 9, 10];
let numerosRepetidos = repetidos(numeros);
console.log(numerosRepetidos)


function generarPassword() {
    function intRandom(min, max) {
        let rnd = Math.random();
        return Math.floor(rnd * (max - min + 1)) + min;
    }

    const caracteres = [
        ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"],
        ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        ["!", "@", "#", "$", "%", "&", "*", "?", "+", "=", "-", "/", "%"]
    ];

    let largo = intRandom(8, 15);
    let pass = new Array(largo);
    pass.fill('');

    pass.forEach((v, i, p) => {
        let lista = caracteres[intRandom(0, caracteres.length - 1)];
        let indice = intRandom(0, lista.length - 1);
        p[i] = lista[indice];
    });

    return pass.join('');
}


function cuentaCaracteres(){
    let palabra = document.getElementById("palabra");
    let resultado = document.getElementById("resultado");

    let textoSinEspacios = palabra.value.replace(/\s/g, "");

    let cantidad = textoSinEspacios.length;


    resultado.textContent = "El número de carácteres es " + cantidad;
}    

function contarPalabras() {
        // Obtén el texto del input
        const textoIngresado = document.getElementById("textoIngresado").value;

        // Divide el texto en palabras usando espacios como separadores
        const palabras = textoIngresado.trim().split(/\s+/); // \s+ cuenta múltiples espacios como uno

        // Contar el número de palabras
        const cantidadDePalabras = palabras.length;

        // Muestra el resultado
        document.getElementById("resultadotexto").innerText = `El texto tiene ${cantidadDePalabras} palabras.`;
    }

function RecargarPage(){
    location.reload()
}
